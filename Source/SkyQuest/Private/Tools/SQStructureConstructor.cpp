// Fill out your copyright notice in the Description page of Project Settings.


#include "Tools/SQStructureConstructor.h"

#include <string>

#include "DrawDebugHelpers.h"
#include "Components/BoxComponent.h"

// Sets default values
ASQStructureConstructor::ASQStructureConstructor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	PMC = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("PMC"));
	PMC->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASQStructureConstructor::BeginPlay()
{
	Super::BeginPlay();

	Blocks.Init(FSQBlockToModify(FIntVector(-1),1,false),StructureSize.X * StructureSize.Y * StructureSize.Z);

	GameInstance = Cast<USQGameInstance>(GetGameInstance());

	UBoxComponent* FrontBox = NewObject<UBoxComponent>(this);
	FrontBox->SetWorldLocation(FVector(StructureSize.X * 1.5,StructureSize.Y / 2,StructureSize.Z / 2) * 100);
	FrontBox->SetBoxExtent(StructureSize * 100 / 2);
	FrontBox->SetCollisionResponseToChannel(ECC_Visibility,ECR_Block);
	FrontBox->RegisterComponent();

	UBoxComponent* BackBox = NewObject<UBoxComponent>(this);
	BackBox->SetWorldLocation(FVector(-StructureSize.X * 0.5,StructureSize.Y / 2,StructureSize.Z / 2) * 100);
	BackBox->SetBoxExtent(StructureSize * 100 / 2);
	BackBox->SetCollisionResponseToChannel(ECC_Visibility,ECR_Block);
	BackBox->RegisterComponent();
	
	UBoxComponent* RightBox = NewObject<UBoxComponent>(this);
	RightBox->SetWorldLocation(FVector(StructureSize.X / 2,StructureSize.Y * 1.5,StructureSize.Z / 2) * 100);
	RightBox->SetBoxExtent(StructureSize * 100 / 2);
	RightBox->SetCollisionResponseToChannel(ECC_Visibility,ECR_Block);
	RightBox->RegisterComponent();

	UBoxComponent* LeftBox = NewObject<UBoxComponent>(this);
	LeftBox->SetWorldLocation(FVector(StructureSize.X / 2,-StructureSize.Y * 0.5,StructureSize.Z / 2) * 100);
	LeftBox->SetBoxExtent(StructureSize * 100 / 2);
	LeftBox->SetCollisionResponseToChannel(ECC_Visibility,ECR_Block);
	LeftBox->RegisterComponent();

	
	UBoxComponent* UpBox = NewObject<UBoxComponent>(this);
	UpBox->SetWorldLocation(FVector(StructureSize.X / 2,StructureSize.Y / 2,StructureSize.Z * 1.5) * 100);
	UpBox->SetBoxExtent(StructureSize * 100 / 2);
	UpBox->SetCollisionResponseToChannel(ECC_Visibility,ECR_Block);
	UpBox->RegisterComponent();

	if (StructureToLoad.BlockToModifies.Num() > 0)
	{
		StructureSize = StructureToLoad.StructureSize;
		Blocks.Init(FSQBlockToModify(FIntVector(-1),1,false),StructureSize.X * StructureSize.Y * StructureSize.Z);
		for (FSQBlockToModify Block : StructureToLoad.BlockToModifies)
		{
			const FIntVector BlockPos = Block.BlockIndex + FIntVector(StructureSize.X / 2, StructureSize.Y / 2, 0);
			const int32 BlockIndex = GetBlockIndex(BlockPos.X,BlockPos.Y,BlockPos.Z);
			Blocks[BlockIndex] = Block;
		}
		RenderChunk();
	}
}

// Called every frame
void ASQStructureConstructor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	DrawDebugBox(GetWorld(),FVector(StructureSize.X / 2,StructureSize.Y / 2,StructureSize.Z / 2) * 100,StructureSize * 100 / 2,FColor::Red);
	DrawDebugBox(GetWorld(),FVector(StructureSize.X / 2,StructureSize.Y / 2,0) * 100,FVector(20),FColor::Blue);	
}

void ASQStructureConstructor::CreateMeshData()
{
	Vertices.Reset();
	Triangles.Reset();
	Normals.Reset();
	UV.Reset();
	Tangents.Reset();
	VertexColors.Reset();
	VertexCount = 0;
	// Sweep over each axis (X, Y, Z)
	for (int Axis = 0; Axis < 3; ++Axis)
	{
		// 2 Perpendicular axis
		const int Axis1 = (Axis + 1) % 3;
		const int Axis2 = (Axis + 2) % 3;

		const int MainAxisLimit = StructureSize[Axis];
		const int Axis1Limit = StructureSize[Axis1];
		const int Axis2Limit = StructureSize[Axis2];

		auto DeltaAxis1 = FIntVector::ZeroValue;
		auto DeltaAxis2 = FIntVector::ZeroValue;

		auto ChunkItr = FIntVector::ZeroValue;
		auto AxisMask = FIntVector::ZeroValue;

		AxisMask[Axis] = 1;

		TArray<FSQMask> Mask;
		Mask.SetNum(Axis1Limit * Axis2Limit);

		// Check each slice of the chunk
		for (ChunkItr[Axis] = -1; ChunkItr[Axis] < MainAxisLimit;)
		{
			int N = 0;

			// Compute Mask
			for (ChunkItr[Axis2] = 0; ChunkItr[Axis2] < Axis2Limit; ++ChunkItr[Axis2])
			{
				for (ChunkItr[Axis1] = 0; ChunkItr[Axis1] < Axis1Limit; ++ChunkItr[Axis1])
				{
					const auto CurrentBlock = GetBlock(ChunkItr);
					const auto CompareBlock = GetBlock(ChunkItr + AxisMask);

					const bool CurrentBlockOpaque = CurrentBlock.BlockType != 1;
					const bool CompareBlockOpaque = CompareBlock.BlockType != 1;

					if (CurrentBlockOpaque == CompareBlockOpaque)
					{
						Mask[N++] = FSQMask{0, 0};
					}
					else if (CurrentBlockOpaque)
					{
						Mask[N++] = FSQMask{CurrentBlock.BlockType, 1};
					}
					else
					{
						Mask[N++] = FSQMask{CompareBlock.BlockType, -1};
					}
				}
			}

			++ChunkItr[Axis];
			N = 0;

			// Generate Mesh From Mask
			for (int j = 0; j < Axis2Limit; ++j)
			{
				for (int i = 0; i < Axis1Limit;)
				{
					if (Mask[N].Normal != 0)
					{
						const auto CurrentMask = Mask[N];
						ChunkItr[Axis1] = i;
						ChunkItr[Axis2] = j;

						int Width;

						for (Width = 1; i + Width < Axis1Limit && CompareMask(Mask[N + Width], CurrentMask); ++Width)
						{
						}

						int Height;
						bool Done = false;

						for (Height = 1; j + Height < Axis2Limit; ++Height)
						{
							for (int k = 0; k < Width; ++k)
							{
								if (CompareMask(Mask[N + k + Height * Axis1Limit], CurrentMask)) continue;

								Done = true;
								break;
							}

							if (Done) break;
						}

						DeltaAxis1[Axis1] = Width;
						DeltaAxis2[Axis2] = Height;

						CreateQuad(
							CurrentMask, AxisMask, Width, Height,
							ChunkItr,
							ChunkItr + DeltaAxis1,
							ChunkItr + DeltaAxis2,
							ChunkItr + DeltaAxis1 + DeltaAxis2
						);

						DeltaAxis1 = FIntVector::ZeroValue;
						DeltaAxis2 = FIntVector::ZeroValue;

						for (int l = 0; l < Height; ++l)
						{
							for (int k = 0; k < Width; ++k)
							{
								Mask[N + k + l * Axis1Limit] = FSQMask{0, 0};
							}
						}

						i += Width;
						N += Width;
					}
					else
					{
						i++;
						N++;
					}
				}
			}
		}
	}
}

void ASQStructureConstructor::RenderChunk()
{
	CreateMeshData();
	
	PMC->CreateMeshSection(0,Vertices,Triangles,Normals,UV,VertexColors,Tangents,true);

	PMC->SetMaterial(0,StructureMat);
}

void ASQStructureConstructor::CreateQuad(const FSQMask Mask, const FIntVector AxisMask, const int Width, const int Height,
	const FIntVector V1, const FIntVector V2, const FIntVector V3, const FIntVector V4)
{
	const auto Normal = FVector(AxisMask * Mask.Normal);

	Vertices.Append({
		FVector(V1) * 100,
		FVector(V2) * 100,
		FVector(V3) * 100,
		FVector(V4) * 100
	});

	Triangles.Append({
		VertexCount,
		VertexCount + 2 + Mask.Normal,
		VertexCount + 2 - Mask.Normal,
		VertexCount + 3,
		VertexCount + 1 - Mask.Normal,
		VertexCount + 1 + Mask.Normal
	});

	int32 DirectionOffset;
	if (Normal.X == 1)
		DirectionOffset = 0;
	
	else if (Normal.X == -1)
		DirectionOffset = 1;
	
	else if (Normal.Y == 1)
		DirectionOffset = 2;
	
	else if (Normal.Y == -1)
		DirectionOffset = 3;
	
	else if (Normal.Z == 1)
		DirectionOffset = 4;
	
	else
		DirectionOffset = 5;

	FSQBlock BlockData;
	GameInstance->GetBlock(Mask.Block,BlockData);
	const FColor VertexColor = FColor(BlockData.AtlasOffsetX[DirectionOffset],BlockData.AtlasOffsetY[DirectionOffset],0,1);
	VertexColors.Append({ VertexColor, VertexColor, VertexColor, VertexColor });

	Normals.Append({
		Normal,
		Normal,
		Normal,
		Normal
	});

	UV.Append({
		FVector2D(0, 0),
		FVector2D(0, 1),
		FVector2D(1, 0),
		FVector2D(1, 1)
	});

	VertexCount += 4;
}

FSQBlockToModify ASQStructureConstructor::GetBlock(const FIntVector Index) const
{
	if (Index.X >= StructureSize.X || Index.Y >= StructureSize.Y || Index.Z >= StructureSize.Z || Index.X < 0 || Index.Y < 0 || Index.Z < 0)
		return FSQBlockToModify();
	return Blocks[GetBlockIndex(Index.X, Index.Y, Index.Z)];
}

int32 ASQStructureConstructor::GetBlockIndex(const int X, const int Y, const int Z) const
{
	return Z * StructureSize.X * StructureSize.Y + Y * StructureSize.X + X; 
}

bool ASQStructureConstructor::CompareMask(const FSQMask M1, const FSQMask M2)
{
	return M1.Block == M2.Block && M1.Normal == M2.Normal;
}

void ASQStructureConstructor::ModifyBlock(const FVector BlockPos, FSQBlockToModify NewBlock)
{
	FIntVector BlockIntIndex = FIntVector(BlockPos / 100);
	const int32 BlockIndex = GetBlockIndex(BlockIntIndex.X,BlockIntIndex.Y,BlockIntIndex.Z);

	BlockIntIndex = BlockIntIndex - FIntVector(StructureSize.X / 2,StructureSize.Y / 2,0);

	if (Blocks.IsValidIndex(BlockIndex))
	{
		NewBlock.BlockIndex = BlockIntIndex;
		Blocks[BlockIndex] = NewBlock;
		RenderChunk();
	}
}

void ASQStructureConstructor::SaveStructure()
{
	FString SaveFile = "\\NewStruct.csv";
	FString FinalString = "(";

	int32 i = 1;
	for (const FSQBlockToModify& Block : Blocks)
	{
		if (Block.BlockIndex != FIntVector(-1))
		{
			FinalString += "(BlockIndex=(X=" + FString::FromInt(Block.BlockIndex.X);
			FinalString += ",Y=" + FString::FromInt(Block.BlockIndex.Y);
			FinalString += ",Z=" + FString::FromInt(Block.BlockIndex.Z);
			FinalString += "),BlockType="+FString::FromInt(Block.BlockType)+",bIsImportant=True)";
			if (i + 1 != Blocks.Num())
				FinalString += ",";
		}
		i++;
	}
	FinalString[FinalString.Len() -1 ] = ' ';
	FinalString += ")";
	FFileHelper::SaveStringToFile(FinalString,*FPaths::Combine(FPaths::ProjectSavedDir(),TEXT("NewStruct.txt")));
}

void ASQStructureConstructor::ClearImportantBlock()
{
	int32 i = 0;
	for (const FSQBlockToModify& Block : Blocks)
	{
		if (Block.BlockType == 1)
			Blocks[i] = FSQBlockToModify(FIntVector(-1),1,false);
		i++;
	}
}

