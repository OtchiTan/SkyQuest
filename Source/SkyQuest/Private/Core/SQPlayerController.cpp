// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SQPlayerController.h"

#include "Kismet/GameplayStatics.h"
#include "World/SQVoxel.h"
#include "World/SQWorldManager.h"
#include "World/SQChunksManager.h"


ASQPlayerController::ASQPlayerController()
{
	ChunksManager = CreateDefaultSubobject<USQChunksManager>(TEXT("ChunksManager"));
}

void ASQPlayerController::BeginPlay()
{
	Super::BeginPlay();

	ChunksManager->PlayerController = this;
	
	const USQGameInstance* GameInstance = Cast<USQGameInstance>(GetWorld()->GetGameInstance());
	ChunksManager->RenderDistance = GameInstance->RenderDistance / 2;
}

void ASQPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (IsValid(GetPawn()))
	{
		const FIntPoint OccupedChunk = USQVoxel::GetChunkIndices(GetPawn()->GetActorLocation());
		if (OccupedChunk != ChunksManager->ActualChunk)
		{
			ChunksManager->ReRenderChunks(OccupedChunk);
		}
	}
}

void ASQPlayerController::Client_GetBlocks_Implementation(FIntPoint ChunkPos)
{
	Server_GetBlocks(ChunkPos);
}

void ASQPlayerController::Server_GetBlocks_Implementation(FIntPoint ChunkPos)
{
	if (!WorldManager)
		WorldManager = Cast<ASQWorldManager>(UGameplayStatics::GetActorOfClass(GetWorld(),ASQWorldManager::StaticClass()));

	const FSQSavedChunk SavedChunk = WorldManager->GetChunkData(ChunkPos);
	
	Server_SetBlocks(ChunkPos,SavedChunk);
}

void ASQPlayerController::Server_SetBlocks_Implementation(FIntPoint ChunkPos, const FSQSavedChunk SavedChunk)
{
	if (ASQChunk* Chunk = ChunksManager->LoadedChunks.FindRef(ChunkPos))
	{
		Chunk->CompressedChunk = SavedChunk.Blocks;

		Chunk->DecompressChunk();

		Chunk->RenderChunk();

		Client_SetBlocks(ChunkPos,SavedChunk);
	}
}

void ASQPlayerController::Client_SetBlocks_Implementation(FIntPoint ChunkPos,const FSQSavedChunk SavedChunk)
{
	if (ASQChunk* Chunk = ChunksManager->LoadedChunks.FindRef(ChunkPos))
	{
		Chunk->CompressedChunk = SavedChunk.Blocks;

		Chunk->DecompressChunk();

		Chunk->RenderChunk();
	}
}
