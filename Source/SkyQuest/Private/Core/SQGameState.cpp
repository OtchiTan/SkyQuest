// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SQGameState.h"

#include "Net/UnrealNetwork.h"

void ASQGameState::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
		Server_SpawnWorld();
}

void ASQGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ASQGameState,WorldManager)
}

void ASQGameState::Server_SpawnWorld_Implementation()
{
	WorldManager = GetWorld()->SpawnActorDeferred<ASQWorldManager>(WorldManagerClass,FTransform(),this,
		nullptr,ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	
	WorldManager->FinishSpawning(FTransform());
}
