// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/SQGameInstance.h"

USQGameInstance::USQGameInstance()
{
	const ConstructorHelpers::FObjectFinder<UDataTable> BlockTableFinder(TEXT("DataTable'/Game/DataTable/blocks.blocks'"));
	FString ContextString;
	if (BlockTableFinder.Object)
	{
		BlockTable = BlockTableFinder.Object;
		TArray<FName> RowNames = BlockTable->GetRowNames();

		for (const FName& Name : RowNames)
		{
			if (const FSQBlock* Block = BlockTable->FindRow<FSQBlock>(Name, ContextString))
			{
				Blocks.Add(*Block);
			}
		}
	}
	const ConstructorHelpers::FObjectFinder<UDataTable> ItemsTableFinder(TEXT("DataTable'/Game/DataTable/items.items'"));
	if (ItemsTableFinder.Object)
	{
		ItemTable = ItemsTableFinder.Object;
		TArray<FName> RowNames = ItemTable->GetRowNames();

		for (const FName& Name : RowNames)
		{
			if (const FSQItemData* Item = ItemTable->FindRow<FSQItemData>(Name, ContextString))
			{
				Items.Add(*Item);
			}
		}
	}
	const ConstructorHelpers::FObjectFinder<UDataTable> StructureTableFinder(TEXT("DataTable'/Game/DataTable/structures.structures'"));
	if (StructureTableFinder.Object)
	{
		StructureTable = StructureTableFinder.Object;
		TArray<FName> RowNames = StructureTable->GetRowNames();

		for (const FName& Name : RowNames)
		{
			if (const FSQWorldTemplate* Structure = StructureTable->FindRow<FSQWorldTemplate>(Name, ContextString))
			{
				Structures.Add(*Structure);
			}
		}
	}
	const ConstructorHelpers::FObjectFinder<UDataTable> RecipeTableFinder(TEXT("DataTable'/Game/DataTable/recipes.recipes'"));
	if (RecipeTableFinder.Object)
	{
		RecipeTable = RecipeTableFinder.Object;
		TArray<FName> RowNames = RecipeTable->GetRowNames();

		for (const FName& Name : RowNames)
		{
			if (const FSQRecipe* Recipe = RecipeTable->FindRow<FSQRecipe>(Name, ContextString))
			{
				Recipes.Add(*Recipe);
			}
		}
	}
}

void USQGameInstance::GetBlock(const int32 ID, FSQBlock& Block) const
{
	if (Blocks.IsValidIndex(ID))
	{
		Block = Blocks[ID];
	}
}

void USQGameInstance::GetItem(const int32 ID, FSQItemData& Item) const
{
	if (Items.IsValidIndex(ID))
	{
		Item = Items[ID];
	}
}

void USQGameInstance::GetStructure(const int32 ID, FSQWorldTemplate& Structure) const
{
	if (Structures.IsValidIndex(ID))
	{
		Structure = Structures[ID];
	}
}

void USQGameInstance::GetRecipe(const int32 ID, FSQRecipe& Recipe) const
{
	if (Recipes.IsValidIndex(ID))
	{
		Recipe = Recipes[ID];
	}
}
