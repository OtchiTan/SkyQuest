// Fill out your copyright notice in the Description page of Project Settings.


#include "World/Usable/SQUtilityBlock.h"

// Sets default values
ASQUtilityBlock::ASQUtilityBlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASQUtilityBlock::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASQUtilityBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

