#include "World/SQChunksManager.h"

#include "World/SQWorldManager.h"
#include "World/SQVoxel.h"

USQChunksManager::USQChunksManager()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void USQChunksManager::BeginPlay()
{
	Super::BeginPlay();

}

void USQChunksManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (WorldManager)
	{
		ASQChunk* Chunk;
		while (ChunksToRender.Dequeue(Chunk))
		{
			Chunk->RenderChunk();
		}
		while (ChunksToShow.Dequeue(Chunk))
		{
			Chunk->ShowChunk();
		}
	}
}

void USQChunksManager::ReRenderChunks(FIntPoint NewChunkPos)
{
	if (!WorldManager) return;

	ActualChunk = NewChunkPos;

	LocationsNeedChunk.Empty();
	ChunksToMove.Empty();
	
	AsyncTask(ENamedThreads::NormalTaskPriority,[this]
	{
		LoadedChunks.GenerateKeyArray(ChunksToMove);
		for (int x = ActualChunk.X - RenderDistance; x < ActualChunk.X + RenderDistance; ++x)
		{
			for (int y = ActualChunk.Y - RenderDistance; y <  ActualChunk.Y + RenderDistance; ++y)
			{
				const FIntPoint ChunkPos = FIntPoint(x,y);
				if (ChunksToMove.Contains(ChunkPos))
					ChunksToMove.Remove(ChunkPos);
				else
					LocationsNeedChunk.Add(ChunkPos);
			}
		}

		AsyncTask(ENamedThreads::GameThread,[this]
		{
			Server_PopulateChunks();
		});
	});
}

void USQChunksManager::MoveChunk(FIntPoint OldPos, FIntPoint NewPos)
{
	ASQChunk* Chunk = LoadedChunks.FindRef(OldPos);
	if (Chunk)
	{
		Chunk->ChunkPos = NewPos;
		const FVector NewLocation = USQVoxel::GetChunkWorldLocation(WorldManager->ChunkSize,NewPos);
		Chunk->Mesh->SetVisibility(false);
		Chunk->SetActorLocation(NewLocation);

		LoadedChunks.Remove(OldPos);
		LoadedChunks.Add(NewPos,Chunk);
		
		PlayerController->Client_GetBlocks(NewPos);
	}
}

void USQChunksManager::Client_SpawnChunks_Implementation(FIntPoint BaseSpawn)
{
	Server_SpawnChunks(BaseSpawn,WorldManager);
}

void USQChunksManager::Server_SpawnChunks_Implementation(FIntPoint BaseSpawn,ASQWorldManager* InWorldManager)
{
	WorldManager = InWorldManager;
	TArray<ASQChunk*> ListSpawnedChunks;
	for (int x = BaseSpawn.X - RenderDistance; x < BaseSpawn.X + RenderDistance; ++x)
	{
		for (int y = BaseSpawn.Y - RenderDistance; y <  BaseSpawn.Y + RenderDistance; ++y)
		{
			const FTransform Transform = FTransform(FRotator(0),FVector(x * 1600, y * 1600, 0), FVector(1));
			ASQChunk* Chunk = GetWorld()->SpawnActorDeferred<ASQChunk>(
				ChunkClass,
				Transform,
				PlayerController,
			nullptr,
				ESpawnActorCollisionHandlingMethod::AlwaysSpawn
				);

			LoadedChunks.Add(FIntPoint(x,y),Chunk);
			ListSpawnedChunks.Add(Chunk);
			
			Chunk->WorldManager = WorldManager;
			Chunk->ChunkPos = FIntPoint(x,y);
			Chunk->ChunkSize = WorldManager->ChunkSize;

			Chunk->FinishSpawning(Transform);
		}
	}
}

void USQChunksManager::Server_PopulateChunks_Implementation()
{
	WorldManager->Server_PopulateChunks(LocationsNeedChunk,PlayerController);
}

void USQChunksManager::Client_OnChunksPopulate_Implementation()
{
	if (LoadedChunks.IsEmpty())
		Client_SpawnChunks(0);
	else
	{
		for (int i = 0; i < LocationsNeedChunk.Num(); ++i)
		{
			if (LocationsNeedChunk.IsValidIndex(i) && ChunksToMove.IsValidIndex(i))
				MoveChunk(ChunksToMove[i],LocationsNeedChunk[i]);
		}
	}
}