// Fill out your copyright notice in the Description page of Project Settings.


#include "World/SQVoxel.h"

FIntPoint USQVoxel::GetChunkIndices(FVector WorldLocation)
{
	if (WorldLocation.X < -1)
		WorldLocation.X++;
	if (WorldLocation.Y < -1)
		WorldLocation.Y++;
	FIntPoint ChunkIndex = FIntPoint(
		FMath::TruncToFloat(WorldLocation.X / 1600),
		FMath::TruncToFloat(WorldLocation.Y / 1600)
		);
	if(WorldLocation.X < 0)
		ChunkIndex.X--;
	if(WorldLocation.Y < 0)
		ChunkIndex.Y--;
	return ChunkIndex;
}

FVector USQVoxel::TruncToBlock(const FVector WorldLocation)
{
	FVector BlockPos =  FVector(
		FMath::TruncToInt(WorldLocation.X / 100) * 100,
		FMath::TruncToInt(WorldLocation.Y / 100) * 100,
		FMath::TruncToInt(WorldLocation.Z / 100) * 100
	);

	
	if(WorldLocation.X < 0)
		BlockPos.X -= 100;
	if(WorldLocation.Y < 0)
		BlockPos.Y -= 100;

	return BlockPos;
}

int32 USQVoxel::GetBlockIndex(FIntVector ChunkSize, FIntVector BlockIndex)
{
	return BlockIndex.Z * ChunkSize.X * ChunkSize.Y + BlockIndex.Y * ChunkSize.X + BlockIndex.X; 
}

FVector USQVoxel::GetChunkWorldLocation(FIntVector ChunkSize,FIntPoint ChunkPos)
{
	return FVector(
		ChunkPos.X * ChunkSize.X,
		ChunkPos.Y * ChunkSize.Y,
		0
	) * 100;
}
