// Fill out your copyright notice in the Description page of Project Settings.


#include "World/SQWorldManager.h"

#include "Core/SQCharacter.h"
#include "Core/SQPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Libraries/SQPerlinLibrary.h"
#include "World/SQChunk.h"
#include "World/SQVoxel.h"
#include "World/SQChunksManager.h"

// Sets default values
ASQWorldManager::ASQWorldManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
	bAlwaysRelevant = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
}

// Called when the game starts or when spawned
void ASQWorldManager::BeginPlay()
{
	Super::BeginPlay();
	
	const auto PlayerController = Cast<ASQPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(),0));

	const auto Character = Cast<ASQCharacter>(PlayerController->GetPawn());

	Character->WorldManager = this;
	PlayerController->WorldManager = this;
	PlayerController->ChunksManager->WorldManager = this;
	
	if (PlayerController->GetNetMode() == NM_ListenServer)
	{
		SpawnBaseWorld();
	}
	else
	{		
		PlayerController->ChunksManager->Client_SpawnChunks(0);
	}
}

// Called every frame
void ASQWorldManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ASQChunk* Chunk;
	while (ChunkToUpdate.Dequeue(Chunk))
	{
		Chunk->AddBlocksToModify();

		Chunk->RenderChunk();
	}
}

void ASQWorldManager::SpawnBaseWorld()
{
	TArray<FIntPoint> ChunksToSpawn;
	
	for (int x = -10; x <= 10; ++x)
	{
		for (int y = -10; y <=  10; ++y)
		{
			ChunksToSpawn.Add(FIntPoint(x,y));
		}
	}

	Server_PopulateChunks(ChunksToSpawn, Cast<ASQPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(),0)));
}

bool ASQWorldManager::ChangeBlock(const FVector WorldLocation, const int32 NewBlock, int32& OldBlock)
{
	ASQChunk* Chunk;
	FIntVector BlockIndex;
	GetBlock(WorldLocation,Chunk,BlockIndex);
	
	if (Chunk)
	{
		Chunk->ModifyBlock(BlockIndex, NewBlock, OldBlock);
		return true;
	}
	
	return false;
}

bool ASQWorldManager::DamageBlock(const FVector WorldLocation, const int32 Damage, int32& BlockDestroyed)
{
	ASQChunk* Chunk;
	FIntVector BlockIndex;
	GetBlock(WorldLocation,Chunk,BlockIndex);
	if (Chunk)
	{
		if (Chunk->DamageBlock(BlockIndex,Damage, BlockDestroyed))
		{
			const FIntPoint ChunkPos = USQVoxel::GetChunkIndices(WorldLocation);
			FSQSavedChunk SavedChunk = SavedChunks.FindRef(ChunkPos);
			SavedChunk.Blocks = CompressChunk(Chunk->BlockList);
			SavedChunks.Add(ChunkPos,SavedChunk);
		}
		return false;
	}

	return false;
}

int32 ASQWorldManager::GetBlock(const FVector WorldLocation, ASQChunk*& Chunk, FIntVector& BlockIndex)
{
	int32 Block = 0;
	// const FIntPoint ChunkPos = USQVoxel::GetChunkIndices(WorldLocation);
	// Chunk = LoadedChunks.FindRef(ChunkPos);
	// if (Chunk)
	// {
	// 	const FVector BlockPos = WorldLocation - Chunk->GetActorLocation();
	// 	BlockIndex = FIntVector(
	// 		FMath::Abs(FMath::TruncToInt(BlockPos.X / 100)),
	// 		FMath::Abs(FMath::TruncToInt(BlockPos.Y / 100)),
	// 		FMath::Abs(FMath::TruncToInt(BlockPos.Z / 100))
	// 		);
	// 	const int32 Index = Chunk->GetBlockIndex(BlockIndex.X,BlockIndex.Y,BlockIndex.Z);
	// 	Block = Chunk->BlockList[Index];
	// }
	return Block;
}

void ASQWorldManager::AddBlockToModify(const FVector WorldLocation, FSQBlockToModify BlockToModify)
{
	ASQChunk* Chunk;
	FIntVector BlockIndex;
	GetBlock(WorldLocation,Chunk,BlockIndex);
	if (Chunk)
	{
		BlockToModify.BlockIndex = BlockIndex;
		if (Chunk->bIsGenerated)
			ChunkToUpdate.Enqueue(Chunk);
		Chunk->BlocksToModify.Enqueue(BlockToModify);
	}
}

FSQSavedChunk ASQWorldManager::GetChunkData(FIntPoint ChunkPos)
{
	const FSQSavedChunk* SavedChunk = SavedChunks.Find(ChunkPos);
	if (SavedChunk)
		return *SavedChunk;

	FSQSavedChunk NewSavedChunk = FSQSavedChunk();
	NewSavedChunk.Blocks = {FSQCompressedBlocks(1,ChunkSize.X*ChunkSize.Y*ChunkSize.Z)};
	return NewSavedChunk;
}

TArray<FSQCompressedBlocks> ASQWorldManager::CompressChunk(const TArray<int32>& Blocks)
{
	TArray<FSQCompressedBlocks> CompressedBlocks = {};
	FSQCompressedBlocks ActualBlock = {Blocks[0],0};
	for (int32 i = 1; i < Blocks.Num(); i++)
	{
		if (ActualBlock.BlockID == Blocks[i])
		{
			ActualBlock.Length++;
		}
		else
		{
			CompressedBlocks.Add(ActualBlock);
			ActualBlock = {Blocks[i],0};
		}
	}
	
	CompressedBlocks.Add(ActualBlock);
	
	return CompressedBlocks;
}

TArray<int32> ASQWorldManager::DecompressChunk(const TArray<FSQCompressedBlocks>& CompressedBlocks)
{
	TArray<int32> Blocks = {};

	for (const FSQCompressedBlocks& CompressedBlock : CompressedBlocks)
	{
		TArray<int32> BlocksToAdd;
		BlocksToAdd.Init(CompressedBlock.BlockID,CompressedBlock.Length + 1);
		Blocks.Append(BlocksToAdd);
	}
	
	return Blocks;
}

void ASQWorldManager::PopulateChunk(const FIntPoint ChunkPos,TArray<int32>& Blocks, TArray<FSQBlockToModify>& BlocksToModify) const
{
	// bool bGenerateTower = false;
	// FIntPoint TowerPos = FIntPoint::ZeroValue;
	//
	// for (const FIntPoint& TowerChunk : TowersChunks)
	// 	if (ChunkPos == TowerChunk)
	// 	{
	// 		bGenerateTower = true;
	// 		TowerPos = FIntPoint(
	// 			FMath::RandRange(0,ChunkSize.X - 1),
	// 			FMath::RandRange(0,ChunkSize.Y - 1)
	// 		);
	// 	}

	Blocks.SetNum(ChunkSize.X * ChunkSize.Y * ChunkSize.Z);
	
	for (int32 x = 0; x < ChunkSize.X; ++x)
		for (int32 y = 0; y < ChunkSize.Y; ++y)
		{
			constexpr int32 HorizonHeight = 128;
			// const float IsleSize = RenderDistance * ChunkSize.X / 2;
			const int32 GlobalX = x + ChunkPos.X * ChunkSize.X;
			const int32 GlobalY = y + ChunkPos.Y * ChunkSize.Y;

			const float PerlinZ = USQPerlinLibrary::TwoD_Perlin_Fractal(GlobalX,GlobalY,1,30,10,2,20);

			// const float DX = FMath::Abs(GlobalX) / IsleSize * 100;
			// const float DY = FMath::Abs(GlobalY) / IsleSize * 100;
			//
			// const float CurveX = PerlinCurve->GetFloatValue(DX);
			// const float CurveY = PerlinCurve->GetFloatValue(DY);

			const int32 MaxZ = FMath::RoundToInt(HorizonHeight + PerlinZ
				// + CurveX + CurveY
				);
			const int32 MinZ = FMath::RoundToInt(HorizonHeight - PerlinZ * 2.5
				// - CurveX * 2.5 - CurveY * 2.5
				- 1);
			

			// const float ThreePerlin = USQPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,8,10);
			// const float ThreePerlinPlacement = USQPerlinLibrary::TwoD_Perlin_Noise(GlobalX,GlobalY,2,20);
			
			for (int32 z = 0; z < ChunkSize.Z; ++z)
			{
				const int32 BlockIndex = USQVoxel::GetBlockIndex(ChunkSize,FIntVector(x,y,z));
				if (z >= MinZ && z <= MaxZ)
				{
					if (z == MaxZ)
					{
						// const int32 DownBlock = GetBlockIndex(x,y,z-1);
						// if (ThreePerlin > 0 && ThreePerlinPlacement > 8.5 && BlockList[DownBlock] == 3)
						// {
						// 	const int32 Structure = FMath::RandRange(0,3);
						// 	SpawnStructure(FIntVector(x,y,z), Structure);
						// }
						// else
							Blocks[BlockIndex] = 0;
					}
					else if (z == MaxZ - 1) Blocks[BlockIndex] = 3;
					else if (z > MaxZ - 4) Blocks[BlockIndex] = 2;
					else Blocks[BlockIndex] = 1;
				}
				else
					Blocks[BlockIndex] = 0;
			}

			// if (bGenerateTower && x == TowerPos.X && y == TowerPos.Y)
			// {
			// 	SpawnStructure(FIntVector(x,y,MaxZ - 4),4);
			// }
		}
}

void ASQWorldManager::AddBlocksToModify(TArray<int32>& Blocks, TArray<FSQBlockToModify>& BlocksToModify) const
{
	for (const FSQBlockToModify& BlockToModify : BlocksToModify)
	{
		const int32 BlockIndex = USQVoxel::GetBlockIndex(ChunkSize,BlockToModify.BlockIndex);

		if (BlockToModify.bIsImportant || !BlockToModify.bIsImportant && Blocks[BlockIndex] == 0)
			Blocks[BlockIndex] = BlockToModify.BlockType;
	}
}

void ASQWorldManager::Server_PopulateChunks_Implementation(const TArray<FIntPoint>& Chunks, ASQPlayerController* PlayerController)
{
	AsyncTask(ENamedThreads::HighTaskPriority,[this,Chunks,PlayerController]
	{
		TArray<FSQSavedChunk> ChunksToAdd;
		for (const FIntPoint& ChunkPos : Chunks)
		{
			if (!SavedChunks.Contains(ChunkPos))
			{
				FSQSavedChunk SavedChunk;
				SavedChunk.ChunkPos = ChunkPos;
				PopulateChunk(ChunkPos,SavedChunk.TempBlocks,SavedChunk.BlocksToModify);
				ChunksToAdd.Add(SavedChunk);
			}
		}

		for (FSQSavedChunk& SavedChunk : ChunksToAdd)
		{
			AddBlocksToModify(SavedChunk.TempBlocks,SavedChunk.BlocksToModify);
			SavedChunk.Blocks = CompressChunk(SavedChunk.TempBlocks);
			SavedChunk.TempBlocks = {};

			SavedChunks.Add(SavedChunk.ChunkPos,SavedChunk);
		}

		AsyncTask(ENamedThreads::GameThread,[PlayerController]
		{
			PlayerController->ChunksManager->Client_OnChunksPopulate();
		});
	});
}
