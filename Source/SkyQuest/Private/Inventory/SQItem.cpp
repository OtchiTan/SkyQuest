// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/SQItem.h"

USQItem* USQItem::CreateItem(FSQItemData& Item, int32 Stack)
{
	USQItem* i = NewObject<USQItem>();
	i->ItemData = Item;
	i->Stack = Stack;
	return i;
}

bool USQItem::IsFullStack()
{
	return Stack == ItemData.MaxStack;
}
