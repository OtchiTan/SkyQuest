// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/SQCrafter.h"

#include "Core/SQGameInstance.h"

// Sets default values for this component's properties
USQCrafter::USQCrafter()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USQCrafter::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USQCrafter::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool USQCrafter::CanCreate(FSQRecipe Recipe)
{
	for (const TPair<int32, int32> &Pair : Recipe.ItemsNeeded)
	{
		if (Inventory->GetQuantityItem(Pair.Key) < Pair.Value)
			return false;
	}
	return true;
}

USQItem* USQCrafter::CraftItem(const int32 RecipeID)
{
	USQItem* ItemCrafted = nullptr;

	const USQGameInstance* GameInstance = Cast<USQGameInstance>(GetWorld()->GetGameInstance());
	
	FSQRecipe RecipeData;
	GameInstance->GetRecipe(RecipeID,RecipeData);
	FSQItemData ItemToCraft;
	GameInstance->GetItem(RecipeData.ItemCrafted,ItemToCraft);

	if (CanCreate(RecipeData))
	{
		ItemCrafted = USQItem::CreateItem(ItemToCraft,RecipeData.QuantityCrafted);
		for (const TPair<int32, int32> &Pair : RecipeData.ItemsNeeded)
		{
			USQItem* ItemTaked;
			Inventory->TakeItem(Pair.Key, Pair.Value, ItemTaked);
		}
	}
	return ItemCrafted;
}

