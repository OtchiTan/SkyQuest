// Fill out your copyright notice in the Description page of Project Settings.


#include "Inventory/SQInventory.h"

#include "Libraries/SQInventoryLibrary.h"

// Sets default values for this component's properties
USQInventory::USQInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void USQInventory::BeginPlay()
{
	Super::BeginPlay();

	Items.SetNum(SizeX * SizeY);
}


// Called every frame
void USQInventory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool USQInventory::AddItemAt(USQItem* ItemToAdd, int32 X, int32 Y, USQItem*& RestItem)
{
	bool bResult;
	const int32 Index = X * SizeY + Y;
	const FSQItemData ItemAsset = ItemToAdd->ItemData;
	if(Items.IsValidIndex(Index))
	{
		USQItem* ItemInSlot = Items[Index];
		if (ItemInSlot == nullptr)
		{
			Items[Index] = ItemToAdd;
			bResult = true;
		}
		else if (ItemAsset.ID == ItemInSlot->ItemData.ID)
		{
			if (ItemInSlot->Stack + ItemToAdd->Stack <= ItemAsset.MaxStack)
			{
				ItemInSlot->Stack += ItemToAdd->Stack;
				bResult = true;
			}
			else
			{
				RestItem = ItemToAdd;
				RestItem->Stack = ItemInSlot->Stack + ItemToAdd->Stack - ItemAsset.MaxStack;
				ItemInSlot->Stack = ItemAsset.MaxStack;
				bResult = false;
			}
		}
		else
		{
			RestItem = ItemInSlot;
			Items[Index] = ItemToAdd;
			bResult = true;
		}
	}
	else
		bResult = false;
	OnInventoryUpdate.Broadcast();
	return bResult;
}

bool USQInventory::RemoveItemAt(int32 X, int32 Y, USQItem*& RemovedItem)
{
	const int32 Index = X * SizeY + Y;
	if (Items.IsValidIndex(Index))
	{
		RemovedItem = Items[Index];
		Items[Index] = nullptr;
		OnInventoryUpdate.Broadcast();
		return true;
	}
	else
	{
		return false;
	}
}

bool USQInventory::AddItem(USQItem* ItemToAdd, USQItem*& RestItem)
{
	bool bSuccess = false;
	if (!IsValid(ItemToAdd))
		return false;
	int32 RestStack = ItemToAdd->Stack;
	for (USQItem* Item : Items)
	{
		if (Item && RestStack > 0)
		{
			if (!Item->IsFullStack() && ItemToAdd->ItemData.ID == Item->ItemData.ID)
			{
				const int32 NewQuantity = RestStack + Item->Stack;
				Item->Stack = FMath::Clamp(NewQuantity,0,Item->ItemData.MaxStack);
				RestStack = NewQuantity - Item->ItemData.MaxStack;
			}
		}
	}
	ItemToAdd->Stack = RestStack;
	if (RestStack > 0)
	{
		for (int x = 0; x < SizeX; ++x)
			for (int y = 0; y < SizeY; ++y)
			{
				const USQItem* Item = Items[x * SizeY + y];
				if (!Item && RestStack > 0)
				{
					AddItemAt(ItemToAdd,x,y,RestItem);
					RestStack = 0;
					bSuccess = true;
					break;
				}
			}
	}
	else
	{
		bSuccess = true;
	}
	if (RestStack > 0)
		RestItem = ItemToAdd;

	OnInventoryUpdate.Broadcast();
	return bSuccess;
}

bool USQInventory::GetAvailableSlot(const int32 ItemID, int32& X, int32& Y)
{
	if (ItemID > 0)
		return false;
	for (int x = 0; x < SizeX; ++x)
	{
		for (int y = 0; y < SizeY; ++y)
		{
			const int32 Index = x * SizeY + y;
			USQItem* ItemInSlot = Items[Index];
			if (ItemInSlot != nullptr && ItemInSlot->ItemData.ID == ItemID && !ItemInSlot->IsFullStack())
			{
				X = x;
				Y = y;
				return true;
			}
		}
	}
	return false;
}

bool USQInventory::GetFreeSlot(int32& X, int32& Y)
{
	for (int x = 0; x < SizeX; ++x)
	{
		for (int y = 0; y < SizeY; ++y)
		{
			const int32 Index = x * SizeY + y;
			if (Items[Index] == nullptr)
			{
				X = x;
				Y = y;
				return true;
			}
		}
	}
	return false;
}

bool USQInventory::TakeItemAt(int32 X, int32 Y, int32 Quantity, USQItem*& QuantityTaked)
{
	const int32 Index = X * SizeY + Y;
	USQItem* ItemInSlot = Items[Index];
	if (ItemInSlot != nullptr)
	{
		if (ItemInSlot->Stack - Quantity == 0)
		{
			QuantityTaked = ItemInSlot;
			Items[Index] = nullptr;
		}
		else if (ItemInSlot->Stack - Quantity > 0)
		{
			QuantityTaked = USQItem::CreateItem(ItemInSlot->ItemData, Quantity);
			Items[Index]->Stack -= Quantity;
		}
		else
		{
			QuantityTaked = USQItem::CreateItem(ItemInSlot->ItemData, ItemInSlot->Stack);
			Items[Index] = nullptr;
		}
		OnInventoryUpdate.Broadcast();
		return true;
	}
	else
		return false;
}

bool USQInventory::TakeItem(int32 ItemType, int32 Quantity, USQItem*& ItemTaked)
{
	int32 i = 0;
	USQInventoryLibrary::CreateItem(ItemType, GetWorld()->GetGameInstance(),0,ItemTaked);
	for (USQItem* Item : Items)
	{
		if (IsValid(Item) && Item->ItemData.ID == ItemType)
		{
			if (Item->Stack > Quantity)
			{
				ItemTaked->Stack = Quantity;
				Item->Stack -= Quantity;
				break;
			}
			else if (Item->Stack == Quantity)
			{
				ItemTaked->Stack = Quantity;
				Items[i] = nullptr;
				break;
			}
			else
			{
				ItemTaked->Stack += Item->Stack;
				Items[i] = nullptr;
			}
		}
		i++;
	}
	
	OnInventoryUpdate.Broadcast();
	return  Quantity <= 0;
}

int32 USQInventory::GetQuantityItem(const int32 Item)
{
	int32 Count = 0;

	for (const USQItem* InventoryItem : Items)
	{
		if (IsValid(InventoryItem) && InventoryItem->ItemData.ID == Item)
			Count += InventoryItem->Stack;
	}
	
	return Count;
}

bool USQInventory::SplitStack(const int32 X,const int32 Y, USQItem*& StackSplited)
{
	const int32 Index = X * SizeY + Y;
	USQItem* ItemInSlot = Items[Index];
	if (ItemInSlot)
	{
		if (ItemInSlot->Stack > 1)
		{
			const int32 QuantityTaked = ItemInSlot->Stack / 2;
			USQInventoryLibrary::CreateItem(ItemInSlot->ItemData.ID,
				GetWorld()->GetGameInstance(), QuantityTaked, StackSplited);
			Items[Index]->Stack -= QuantityTaked;
		}
		else
		{
			StackSplited = ItemInSlot;
			Items[Index] = nullptr;
		}
			OnInventoryUpdate.Broadcast();
		return true;
	}
	else
		return false;
}

bool USQInventory::GetQuantityInItem(USQItem* Item, const int32 QuantityToSplit, USQItem*& OriginStack, USQItem*& NewStack)
{
	if (Item)
	{
		OriginStack = Item;
		if (Item->Stack > QuantityToSplit)
		{
			USQInventoryLibrary::CreateItem(Item->ItemData.ID,
				GetWorld()->GetGameInstance(), QuantityToSplit, NewStack);
			OriginStack->Stack -= QuantityToSplit;
		}
		else
		{
			NewStack = OriginStack;
			OriginStack = nullptr;
		}
		OnInventoryUpdate.Broadcast();
		return true;
	}
	else
		return false;
}
