// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "Inventory/Structs/SQItemData.h"
#include "Inventory/Structs/SQRecipe.h"
#include "World/Structs/SQBlock.h"
#include "World/Structs/SQWorldTemplate.h"
#include "SQGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SKYQUEST_API USQGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	USQGameInstance();

	UPROPERTY(BlueprintReadOnly)
	UDataTable* BlockTable;
	
	UPROPERTY(BlueprintReadOnly)
	UDataTable* ItemTable;
	
	UPROPERTY(BlueprintReadOnly)
	UDataTable* StructureTable;
	
	UPROPERTY(BlueprintReadOnly)
	UDataTable* RecipeTable;

	UPROPERTY(BlueprintReadOnly)
	TArray<FSQBlock> Blocks;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FSQItemData> Items;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FSQWorldTemplate> Structures;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FSQRecipe> Recipes;

	UFUNCTION(BlueprintCallable,BlueprintPure)
	void GetBlock(int32 ID,FSQBlock& Block) const;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	void GetItem(int32 ID,FSQItemData& Item) const;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	void GetStructure(int32 ID,FSQWorldTemplate& Structure) const;
	
	UFUNCTION(BlueprintCallable,BlueprintPure)
	void GetRecipe(int32 ID, FSQRecipe& Recipe) const;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	int32 RenderDistance = 16;
};
