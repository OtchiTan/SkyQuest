// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "World/SQChunk.h"
#include "World/Structs/SQSavedChunk.h"
#include "SQPlayerController.generated.h"

class ASQWorldManager;
class USQChunksManager;

/**
 * 
 */
UCLASS()
class SKYQUEST_API ASQPlayerController : public APlayerController
{
	GENERATED_BODY()

	ASQPlayerController();
	
public:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	USQChunksManager* ChunksManager;

	UPROPERTY()
	ASQWorldManager* WorldManager;

	UFUNCTION(Client,Reliable)
	void Client_GetBlocks(FIntPoint ChunkPos);
	
	UFUNCTION(Server,Reliable)
	void Server_GetBlocks(FIntPoint ChunkPos);
	
	UFUNCTION(Server,Reliable)
	void Server_SetBlocks(FIntPoint ChunkPos,const FSQSavedChunk SavedChunk);
	
	UFUNCTION(Client,Reliable)
	void Client_SetBlocks(FIntPoint ChunkPos,const FSQSavedChunk SavedChunk);
};
