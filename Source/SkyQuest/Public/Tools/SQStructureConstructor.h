// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "Core/SQGameInstance.h"
#include "GameFramework/Actor.h"
#include "World/Structs/SQBlockToModify.h"
#include "World/Structs/SQMask.h"
#include "World/Structs/SQWorldTemplate.h"
#include "SQStructureConstructor.generated.h"

UCLASS()
class SKYQUEST_API ASQStructureConstructor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASQStructureConstructor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	const FVector BlockVertexData[8] = {
		FVector(100,100,100),
		FVector(100,0,100),
		FVector(100,0,0),
		FVector(100,100,0),
		FVector(0,0,100),
		FVector(0,100,100),
		FVector(0,100,0),
		FVector(0,0,0)
	};

	const int BlockTriangleData[24] = {
		0,1,2,3, // Forward
		5,0,3,6, // Right
		4,5,6,7, // Back
		1,4,7,2, // Left
		5,4,1,0, // Up
		3,2,7,6  // Down
	};

	int32 VertexCount = 0;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	FVector StructureSize = FVector(51);

	UPROPERTY(BlueprintReadWrite)
	TArray<FSQBlockToModify> Blocks;

	UPROPERTY()
	UProceduralMeshComponent* PMC;
	
	void CreateMeshData();

	UFUNCTION(BlueprintCallable)
	void RenderChunk();

	void CreateQuad(const FSQMask Mask,const FIntVector AxisMask,const int Width,const int Height,
		const FIntVector V1,const FIntVector V2,const FIntVector V3,const FIntVector V4);
	
	UFUNCTION(BlueprintCallable)
	FSQBlockToModify GetBlock(const FIntVector Index) const;
	
	int32 GetBlockIndex(const int X, const int Y, const int Z) const;
	
	static bool CompareMask(const FSQMask M1, const FSQMask M2);

	UFUNCTION(BlueprintCallable)
	void ModifyBlock(FVector BlockPos, FSQBlockToModify NewBlock);

	UFUNCTION(BlueprintCallable)
	void SaveStructure();

	UFUNCTION(BlueprintCallable)
	void ClearImportantBlock();
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	UMaterial* StructureMat;

	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	FSQWorldTemplate StructureToLoad;

	UPROPERTY()
	USQGameInstance* GameInstance;
	
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UV;
	TArray<FColor> VertexColors;
	TArray<FProcMeshTangent> Tangents;
};
