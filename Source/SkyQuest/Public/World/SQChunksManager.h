// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Core/SQPlayerController.h"
#include "SQChunksManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SKYQUEST_API USQChunksManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USQChunksManager();
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	TArray<FIntPoint> ChunksToMove;
	TArray<FIntPoint> LocationsNeedChunk;

	UPROPERTY()
	TMap<FIntPoint,ASQChunk*> LoadedChunks;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<ASQChunk> ChunkClass;
	
	TQueue<ASQChunk*,EQueueMode::Mpsc> ChunksToRender;
	TQueue<ASQChunk*,EQueueMode::Mpsc> ChunksToShow;

	UPROPERTY()
	ASQPlayerController* PlayerController;

	UPROPERTY()
	ASQWorldManager* WorldManager;
	
	FIntPoint ActualChunk;
	int32 RenderDistance = 8;

	void ReRenderChunks(FIntPoint NewChunkPos);
	
	void MoveChunk(FIntPoint OldPos,FIntPoint NewPos);

	UFUNCTION(Client,Reliable)
	void Client_SpawnChunks(FIntPoint BaseSpawn);
	
	UFUNCTION(Server,Reliable)
	void Server_SpawnChunks(FIntPoint BaseSpawn,ASQWorldManager* InWorldManager);
	
	UFUNCTION(Server,Unreliable)
	void Server_PopulateChunks();

	UFUNCTION(Client,Reliable)
	void Client_OnChunksPopulate();
};
