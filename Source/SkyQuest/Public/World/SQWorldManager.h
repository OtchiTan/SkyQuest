// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SQChunk.h"
#include "GameFramework/Actor.h"
#include "Structs/SQBlockToModify.h"
#include "Structs/SQSavedChunk.h"
#include "SQWorldManager.generated.h"

class ASQChunk;
class ASQPlayerController;

UCLASS()
class SKYQUEST_API ASQWorldManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASQWorldManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere)
	UCurveFloat* PerlinCurve;
	
public:

	TQueue<ASQChunk*,EQueueMode::Mpsc> ChunkToUpdate;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void SpawnBaseWorld();

	UPROPERTY()
	TMap<FIntPoint, FSQSavedChunk> SavedChunks;

	UFUNCTION(BlueprintCallable)
	bool ChangeBlock(FVector WorldLocation, int32 NewBlock, int32& OldBlock);

	UFUNCTION(BlueprintCallable)
	bool DamageBlock(FVector WorldLocation, int32 Damage, int32& BlockDestroyed);

	UFUNCTION(BlueprintCallable,BlueprintPure)
	int32 GetBlock(FVector WorldLocation, ASQChunk*& Chunk, FIntVector& BlockIndex);

	void AddBlockToModify(FVector WorldLocation, FSQBlockToModify BlockToModify);

	FSQSavedChunk GetChunkData(FIntPoint ChunkPos);

	UFUNCTION(Server,Reliable)
	void Server_PopulateChunks(const TArray<FIntPoint>& Chunks, ASQPlayerController* PlayerController);

	static TArray<FSQCompressedBlocks> CompressChunk(const TArray<int32>& Blocks);

	static TArray<int32> DecompressChunk(const TArray<FSQCompressedBlocks>& CompressedBlocks);
	
	void PopulateChunk(const FIntPoint ChunkPos,TArray<int32>& Blocks, TArray<FSQBlockToModify>& BlocksToModify) const;

	void AddBlocksToModify(TArray<int32>& Blocks,TArray<FSQBlockToModify>& BlocksToModify) const;
	
	UPROPERTY(BlueprintReadWrite,EditAnywhere)
	FIntVector ChunkSize = FIntVector(16,16,256);	
};
