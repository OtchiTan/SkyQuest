﻿#pragma once

#include "SQCompressedBlocks.generated.h"

USTRUCT(BlueprintType)
struct FSQCompressedBlocks
{
	GENERATED_BODY()

	UPROPERTY()
	int32 BlockID = 0;

	UPROPERTY()
	int32 Length = 1;

	FSQCompressedBlocks(){}

	FSQCompressedBlocks(int32 InBlockID,int32 InLength)
	{
		BlockID = InBlockID;
		Length = InLength;
	}
};