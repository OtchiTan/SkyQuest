﻿#pragma once

#include "SQMask.generated.h"


USTRUCT()
struct FSQMask
{
	GENERATED_BODY()
	
	int32 Block;
	int32 Normal;
};