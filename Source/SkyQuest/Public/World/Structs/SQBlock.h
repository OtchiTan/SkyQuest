﻿#pragma once

#include "World/Usable/SQUtilityBlock.h"
#include "Engine/DataTable.h"
#include "SQBlock.generated.h"

USTRUCT(BlueprintType)
struct SKYQUEST_API FSQBlock : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString Name;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName DisplayName;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMesh* StaticMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 MaxHealth = 3;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Hardness = 1;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<int32> AtlasOffsetX = {0,0,0,0,0,0};
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<int32> AtlasOffsetY = {0,0,0,0,0,0};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ItemID;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FName EfficiencyTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	bool bIsUsable = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<ASQUtilityBlock> UtilityBlockClass;
};