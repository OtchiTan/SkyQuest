﻿#pragma once

#include "SQBlockToModify.h"
#include "SQCompressedBlocks.h"
#include "SQSavedChunk.generated.h"

USTRUCT(BlueprintType)
struct SKYQUEST_API FSQSavedChunk
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FIntPoint ChunkPos;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FSQCompressedBlocks> Blocks;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<FSQBlockToModify> BlocksToModify;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<int32> TempBlocks; // Variable temporaire pour la population des chunks avec les BlocksToModify
};