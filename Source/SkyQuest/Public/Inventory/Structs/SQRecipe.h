﻿#pragma once
#include "Engine/DataTable.h"

#include "SQRecipe.generated.h"

USTRUCT(BlueprintType)
struct SKYQUEST_API FSQRecipe : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString Name;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FName DisplayName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 CraftLevel = 1;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 ItemCrafted;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 QuantityCrafted = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TMap<int32, int32> ItemsNeeded;
};